#ifndef ZOMBIE_H
#define ZOMBIE_H
#include <SDL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include "objloader.h"
#include "vector3d.h"
#include "collisionplane.h"
#include "collisionsphere.h"
#include "camera.h"
#include "functions.h"
#include "collision.h"
#include "level.h"
#include "player.h"
#include "weapon.h"

class zombie {
	std::vector<unsigned int> frames;
	unsigned int walk, attack, die;
	unsigned int curframe;

	int health, strenght;
	float speed;
	vector3d direction, rotation;
	bool iswalk, isattack, isdead;

	collisionsphere cs;

public:
	zombie(std::vector<unsigned int>& anim, unsigned w,
		unsigned int a, unsigned int d, int h, int str,
		float sp, collisionsphere ccs);
	~zombie(){}
	bool update(std::vector<collisionplane>& col, vector3d playerloc);
	void show();
	bool setAttack(collisionsphere player);
	collisionsphere* getCollisionSphere();
	void setLocation(vector3d newloc);
	void decreaseHealth(int num);
	int getHealth();
	int getStrenght();
	bool isDead();

 };
#endif
