#ifndef CAMERA_H
#define CAMERA_H
#include <cmath>
#include <SDL.h>
#include <GL/glew.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <iostream>
#include "vector3d.h"

class camera {
	vector3d loc;
	float camPitch;
	float camYaw;
	float movevel;
	float mousevel;
	bool mi;
	void lockCamera();
	void moveCamera(const float& dir);
	void moveCameraUp(const float& dir);
public:
	camera();
	camera(vector3d loc);
	camera(vector3d loc, float yaw, float pitch);
	camera(vector3d loc, float yaw, float pitch, float mv, float mov);
	void Control();
	void UpdateCamera();
	vector3d getVector();
	vector3d getLocation();
	float getPitch();
	float getYaw();
	float getMovevel();
	float getMousevel();
	bool isMouseIn();

	void setLocation(vector3d vec);
	void lookAt(float pitch, float yaw);
	void mouseIn(bool b);
	void setSpeed(float mv, float mov);

};
#endif 

