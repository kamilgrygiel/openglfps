#include "weapon.h"

weapon::weapon(std::vector<unsigned int>& anim, unsigned int ol,
	unsigned int na, unsigned int fa, unsigned int ra,
	vector3d pos, vector3d rot, vector3d apos,
	vector3d arot, float prec, float aprec, int str,
	int maxb, int allbul, int sp, const char* nam,
	bool isa) {
	frames = anim;
	outerview = ol;
	normalanimation = na;
	reloadanimation = ra;
	fireanimation = fa;
	precision = (prec != 0 ? prec : 0.000001);
	aimprecision = (aprec != 0 ? aprec : 0.000001);
	position = pos;
	rotation = rot;
	aimposition = apos;
	aimrotation = arot;
	strength = str;
	allbullets = allbul;
	maxBulletsInMag = maxb;
	speed = sp;
	name = nam;
	position_expected = position;
	rotation_expected = rotation;

	curpos = position;
	currot = rotation;

	isaim = false;
	isreloading = false;
	isautomatic = isa;
	isfired = false;
	bulletsInMag = maxBulletsInMag;

	lastshot = 1000;
	curframe = 0;
	curmode = 1;
}
weapon::~weapon() {

}
void weapon::update() {
	lastshot++;
	curframe++;
	if (curmode == 1) {
		if (curframe >= normalanimation)
			curframe = 0;
	}
	else if (curmode == 2) {
		if (curframe >= normalanimation + fireanimation) {
				curmode = 1;
				curframe = 0;
			
		}

	}
	else if (curmode == 3) {
		if (curframe >= normalanimation + fireanimation + reloadanimation) {
			curmode = 1;
			curframe = 0;
			isreloading = false;
		}
	}
	vector3d tmpVec(position_expected - curpos);
	tmpVec.normalize();
	tmpVec *= 0.3;
	curpos += tmpVec;
	if (std::abs(position_expected.x - curpos.x) < 0.3 &&
		std::abs(position_expected.y - curpos.y) < 0.3 &&
		std::abs(position_expected.z - curpos.z) < 0.3)
		curpos = position_expected;

	tmpVec.change(rotation_expected - currot);
	tmpVec.normalize();
	tmpVec *= 0.3;
	curpos += tmpVec;
	if (std::abs(rotation_expected.x - currot.x) < 0.3 &&
		std::abs(rotation_expected.y - currot.y) < 0.3 &&
		std::abs(rotation_expected.z - currot.z) < 0.3)
		currot = rotation_expected;
}
void weapon::show() {
	glPushMatrix();
	glTranslatef(curpos.x, curpos.y, curpos.z);
	glRotatef(currot.x, 1, 0, 0);
	glRotatef(currot.y, 0, 1, 0);
	glRotatef(currot.z, 0, 0, 1);
	glCallList(frames[curframe]);
	glPopMatrix();
}
bool weapon::fire(vector3d& direction, vector3d camdirection) {
	if (isreloading)
		return false;
	if ((!isautomatic && !isfired) || isautomatic) {
		if (lastshot >= speed) {
			if (bulletsInMag > 0) {
				if (isaim) {
					direction.x = camdirection.x;
					direction.y = camdirection.y;
					direction.z = camdirection.z;
				}
				else {
					direction.x = camdirection.x;
					direction.y = camdirection.y;
					direction.z = camdirection.z;
				}
				isfired = true;
				lastshot = 0;
				bulletsInMag--;
				curframe = normalanimation;
				curmode = 2;
				return true;
			}
			else {
				reload();
				return false;
			}
		}
	}
	return 0;
}
void weapon::stopfire() {
	isfired = false;
}
void weapon::reload() {
	if (!isreloading && maxBulletsInMag != bulletsInMag) {
		isreloading = true;
		if (allbullets > maxBulletsInMag - bulletsInMag) {
			allbullets -= maxBulletsInMag - bulletsInMag;
			bulletsInMag = maxBulletsInMag;
		}
		else {
			bulletsInMag = allbullets + bulletsInMag;
			allbullets = 0;
		}
		curframe = normalanimation + fireanimation;
		curmode = 3;
	}
}
void weapon::aim() {
	isaim != isaim;
	if (isaim) {
		rotation_expected = aimrotation;
		position_expected = aimposition;
	}
	else {
		rotation_expected = rotation;
		position_expected = position;
	}
}


void weapon::addBullets(unsigned int num) {
	allbullets += num;
}
void weapon::setBullets(unsigned int num) {
	allbullets = num;
}
int weapon::getStrength() {
	return strength;
}
int weapon::getAmmo() {
	return bulletsInMag;
}
int weapon::getAllAmmo() {
	return allbullets;
}
std::string weapon::getName() {
	return name;
}
std::vector<unsigned int>& weapon::getAnimation() {
	return frames;
}
bool weapon::isAimed() {
	return isaim;
}
unsigned int weapon::getOuterView() {
	return outerview;
}