#ifndef LEVEL_H
#define LEVEL_H
#include <SDL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include "objloader.h"
#include "vector3d.h"
#include "collisionplane.h"
#include "collisionsphere.h"
#include "camera.h"
#include "functions.h"
#include "collision.h"

class level {
	unsigned int mesh;
	std::vector<collisionplane> cp;
	std::vector<vector3d> spawnPoints;
	std::string name;
public:
	level(const char* c, unsigned int map, std::vector<collisionplane>& cplanes,
		std::vector<vector3d>& sp);
	~level();
	void update();
	void show();
	std::vector<collisionplane>& getCollisionPlanes();
	std::string& getName();
	void change(const char* c, unsigned int map, std::vector<collisionplane>& cplanes,
		std::vector<vector3d>& sp);
	std::vector<vector3d>& getSpawnPoints();
};

#endif 

