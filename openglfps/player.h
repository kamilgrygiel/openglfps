#ifndef PLAYER_H
#define PLAYER_H
#include <SDL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <vector>
#include <string>
#include "vector3d.h"
#include "collisionplane.h"
#include "collisionsphere.h"
#include "camera.h"
#include "collision.h"
#include "weapon.h"


class player {
	int id;
	bool isready;
	std::string name;
	collisionsphere cs;
	int health;
	vector3d force; //gravity
	vector3d direction;
	bool isground;
	bool iscollision, issprint;
	float sprintspeed, normalspeed;
	float lookspeed;
	float energy;
	int points;
	std::vector<weapon*> weapons;
	int curweapon;
	bool isWeapon;
public:
	camera cam;
	player(const char* n, collisionsphere ccs, float sprints,
		float normals, float looks, weapon* wep);
	~player();
	void update(std::vector<collisionplane>& collplane);
	void show();
	void setPosition(vector3d pos);
	
	std::string getName();
	int getHealth();
	collisionsphere getCollisionSphere();
	int getPoints();

	void setHealth(int h);
	void addPoints(int n);

	void addWeapon(weapon* wep);
	void changeWeapon(int num);
	void changeWeaponDown();
	void changeWeaponUp();
	weapon* getCurrentWeapon();
	void haveWeapon(bool b);

	void setId(int i);
	int getId();
	bool isReady();
	int getCurFrame();
	vector3d getRotation();
	int getWeaponIndex();
};

#endif
