#include "game.h"

game::game() {
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Surface* screen = SDL_SetVideoMode
	(640, 480, 32, SDL_SWSURFACE | SDL_OPENGL);
	glClearColor(0.5, 0.5, 0.5, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, 640.0 / 480.0, 1.0, 500.0);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	initskybox();
	std::vector<collisionplane> mapcp;
	std::vector<vector3d> mapsp;
	mapsp.push_back(vector3d(3, 4, 5));
	unsigned int map = obj.load("untitled.obj", &mapcp);
	levels.push_back(new level("name", map, mapcp, mapsp));
	std::vector<unsigned int> anim;
	loadAnimation(anim, "data/weapon1/weapon", 38);
	weapons.push_back(new weapon(anim, anim[0], 1, 16, 19, vector3d(0.5, -0.3, -1.96),
		vector3d(0, -85.6, 0), vector3d(-0.1, -0.44, -1.96), vector3d(0, -85.6, 0), 100, 1000,
		10, 10, 300, 20, "weapon1", 1));
	player1 = new player("player1", collisionsphere(vector3d(0, 100, 0), 2.0), 0.2, 0.2, 0.2,weapons[0]);
	anim.clear();
	loadAnimation(anim, "data/zombie1/zombie", 60);
	zombies.push_back(new zombie(anim, 30, 20, 10, 100, 5, 0.1,
		collisionsphere(vector3d(5, 10, 0), 2.0)));
}

game::~game() {
	for (int i = 0; i < levels.size(); i++)
		delete levels[i];
	for (int i = 0; i < weapons.size(); i++)
		delete weapons[i];
	for (int i = 0; i < zombies.size(); i++)
		delete zombies[i];
	SDL_Quit();
	killskybox();
}

void game::start() {
	Uint32 start;
	SDL_Event event;
	bool running = true;
	vector3d direction;
	bool mousebuttondown = false;
	while (running)
	{
		start = SDL_GetTicks();
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				running = false;
				break;
			case SDL_MOUSEBUTTONDOWN:
				player1->cam.mouseIn(true);
				SDL_ShowCursor(SDL_DISABLE);
				if (event.button.button == SDL_BUTTON_LEFT) {
					mousebuttondown = true;
				}
				else if (event.button.button == SDL_BUTTON_RIGHT)
					player1->getCurrentWeapon()->aim();
				else if (event.button.button == SDL_BUTTON_WHEELUP)
					player1->changeWeaponUp();
				else if (event.button.button == SDL_BUTTON_WHEELDOWN)
					player1->changeWeaponDown();
				break;
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					running = false;
					break;
				} 
				switch (event.key.keysym.sym) {
				case SDLK_0:
					player1->changeWeapon(0);
					break;
				case SDLK_1:
					player1->changeWeapon(1);
					break;
				case SDLK_2:
					player1->changeWeapon(2);
					break;
				case SDLK_r:
					player1->getCurrentWeapon()->reload();
				}
				break;
			case SDL_MOUSEBUTTONUP:
				mousebuttondown = false;
				player1->getCurrentWeapon()->stopfire();
				break;
			}
		}
		if (mousebuttondown) {
			if (player1->getCurrentWeapon()->fire(direction, player1->cam.getVector())) {
				for (int i = 0; i < zombies.size(); i++) {
					if (collision::raysphere(zombies[i]->getCollisionSphere()->center.x,
						zombies[i]->getCollisionSphere()->center.y,
						zombies[i]->getCollisionSphere()->center.z,
						direction.x, direction.y, direction.z,
						player1->getCollisionSphere().center.x, player1->getCollisionSphere().center.y,
						player1->getCollisionSphere().center.z, zombies[i]->getCollisionSphere()->r)) {
						zombies[i]->decreaseHealth(player1->getCurrentWeapon()->getStrength());

					}
				}
			}
		}
		update();
		show();

		SDL_GL_SwapBuffers();
		if (1000 / 30>(SDL_GetTicks() - start))
			SDL_Delay(1000 / 30 - (SDL_GetTicks() - start));
	}
}

void game::update() {
	for (int i = 0; i < levels.size(); i++) {
		levels[i]->update();
	}
	player1->update(levels[0]->getCollisionPlanes());
	for (int i = 0; i < zombies.size(); i++) {
		if (zombies[i]->update(levels[0]->getCollisionPlanes(),
			player1->getCollisionSphere().center)) {
			//zombie died
		}
	}
	for (int i = 0; i < zombies.size(); i++) {
		if (zombies[i]->setAttack(player1->getCollisionSphere())) {
			//zombie attack
			player1->setHealth(player1->getHealth() - zombies[i]->getStrenght());
		}
	}
}

void game::show() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	player1->show();
	player1->cam.Control();
	drawSkybox(500.0);
	player1->cam.UpdateCamera();
	
	for (int i = 0; i < levels.size(); i++) {
		levels[i]->show();
	}

	for (int i = 0; i < zombies.size(); i++) {
		zombies[i]->show();
	}
}
void game::loadAnimation(std::vector<unsigned int>& anim,
	std::string filename, unsigned int num) {
	char tmp[200];
	for (int i = 1; i <= num; i++) {
		if (i < 10)
			sprintf(tmp, "_00000%d", i);
		else if(i<100)
			sprintf(tmp, "_0000%d", i);
		else if(i<1000)
			sprintf(tmp, "_000%d", i);
		else if(i<10000)
			sprintf(tmp, "_00%d", i);
		else if(i<100000)
			sprintf(tmp, "_0%d", i);
		else if(i<1000000)
			sprintf(tmp, "_%d", i);
		std::string tmp2(filename + tmp);
		tmp2 += ".obj";
		unsigned int id = obj.load(tmp2);
		anim.push_back(id);

	}
}