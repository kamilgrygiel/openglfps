#ifndef GAME_H
#define GAME_H
#include <SDL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <vector>
#include "objloader.h"
#include "vector3d.h"
#include "collisionplane.h"
#include "collisionsphere.h"
#include "camera.h"
#include "functions.h"
#include "collision.h"
#include "level.h"
#include "player.h"
#include "weapon.h"
#include "zombie.h"
#include "network.h"
#include "enemy.h"

class game {
	objloader obj;
	std::vector<level*> levels;
	std::vector<weapon*> weapons;
	std::vector<zombie*> zombies;
	std::vector<enemy*> enemies;
	std::vector<unsigned int> enemyframe;
	player* player1;

	bool isOnline;
	network* net;

	void update();
	void show();
	unsigned int loadTexture(const char * filename);
	void loadAnimation(std::vector<unsigned int>& anim,
		std::string filename, unsigned int num);

public:
	game();
	~game();
	void start();
};

#endif

