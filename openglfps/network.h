#ifndef NETWORK_H
#define NETWORK_H
#include <SDL_net.h>
#include "player.h"
#include "enemy.h"
#include "functions.h"
#include <vector>
#include <cstring>


#define SDL_reinterpret_cast(type, expression) reinterpret_cast<type>(expression)

class network {
	SDLNet_SocketSet server;
	TCPsocket connection;
	char tmp[1400];
public:
	network(const char* ip);
	~network();
	void send(player* p);
	void sendShot(player* p, int id);
	void recv(std::vector<enemy*>& enemies, std::vector<weapon*> weapons,
		player* p, std::vector<unsigned int> f);
};

#endif 