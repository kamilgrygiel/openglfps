#include "network.h"


network::network(const char* ipchar) {
	SDLNet_Init();
	IPaddress ip;
	if (SDLNet_ResolveHost(&ip, ipchar, 1234) == -1)
		std::cout << "error" << std::endl;
	connection = SDLNet_TCP_Open(&ip);
	if (connection == NULL)
		std::cout << "error" << std::endl;
	server = SDLNet_AllocSocketSet(1);
	SDLNet_TCP_AddSocket(server, connection);
}
network::~network() {
	SDLNet_TCP_Send(connection, "2 \n", 4);
	SDLNet_TCP_Close(connection);
	SDLNet_FreeSocketSet(server);
	SDLNet_Quit();
}
void network::send(player* p) {
	if (p->isReady()) {
		vector3d vec = p->cam.getVector();
		sprintf(tmp, "1 %d %d %f %f %f %f %f %f %f %f %f %d %d \n",
			p->getId(), p->getCurFrame(), p->getCollisionSphere().center.x,
			p->getCollisionSphere().center.y, p->getCollisionSphere().center.z,
			p->getRotation().x, p->getRotation().y, p->getRotation().z,
			vec.x, vec.y, vec.z, p->getHealth(), p->getWeaponIndex());

		int size = 0;
		int len = strlen(tmp) + 1;
		while (size < len) {
			size += SDLNet_TCP_Send(connection, tmp + size, len - size);
		}
	}
}
void network::sendShot(player* p, int id) {
	if (p->isReady()) {
		sprintf(tmp, "3 %d %d %d \n", id, p->getCurrentWeapon()->getStrength(),
			p->getId());
		int size = 0;
		int len = strlen(tmp) + 1;
		while (size < len) {
			size += SDLNet_TCP_Send(connection, tmp + size, len - size);
		}
	}
}
/*void network::recv(std::vector<enemy*>& enemies, std::vector<weapon*> weapons,
	player* p, std::vector<unsigned int> f) {
	while (SDLNet_CheckSockets(server, 0) > 0 && SDLNet_SocketReady(connection)) {
		int offset = 0;
		do {
			offset += SDLNet_TCP_Recv(connection, tmp + offset, 1400);
			if (offset <= 0)
				return;
		} while (tmp[strlen(tmp) - 1] != '\n');
		int type, id;
		sscanf(tmp, "%d %d", &type, &id);
		if (type == 0) {
			p->setId(id);
		}
		else if (type == 1) {
			int i;
			for(i=0;i<enemies.size();i++)
				if (enemies[i]->id == id) {
					int tmp2;
					sscanf(tmp, "1 %d %d %f %f %f %f %f %f %f %f %f %d %d \n",
						&tmp2, &(enemies[i]->curframe), &(enemies[i]->position.x),
						&(enemies[i]->position.y), &(enemies[i]->position.z),
						&(enemies[i]->rotation.x), &(enemies[i]->rotation.y),
						&(enemies[i]->rotation.z), &(enemies[i]->lookDirection.x),
						&(enemies[i]->lookDirection.y), &(enemies[i]->lookDirection.z),
						&(enemies[i]->health), &tmp2);
					enemies[i]->curweapon = weapons[tmp2];
					break;
				}
			if (i >= enemies.size())
				enemies.push_back(new enemy(f, id));
		}
		else if (type == 2) {
			for (int i = 0; i < enemies.size(); i++)
				if (enemies[i]->id == id)
					enemies.erase(enemies.begin() + 1);
		}
		else if (type == 3) {
			int tmp1, tmp2, tmp3;
			sscanf(tmp, "3 %d %d %d", &tmp1, &tmp2, &tmp3);
			p->setHealth(p->getHealth() - tmp2);
		}
	}
}*/
