//This example program is created by thecplusplusuy for demonstration purposes. It's a simple skybox:
//http://www.youtube.com/user/thecplusplusguy
//Free source, modify if you want, LGPL licence (I guess), I would be happy, if you would not delete the link
//so other people can see the tutorial
//this file is functions.h, it's contain the function prototypes
#include <SDL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <cmath>
#include "objloader.h"
#include "vector3d.h"
#ifndef FUNCTION_H
#define FUNCTION_H
#ifndef M_PI
#define M_PI 3.1415926535
#endif



void drawCube(float size);
void drawSkybox(float size);	//draw the skybox
void initskybox();	//load the textures
void killskybox();	//delete the textures
unsigned int loadTexture(const char*);
vector3d getcamcoord();
void setcamcoord(float x,float y,float z);
#endif
