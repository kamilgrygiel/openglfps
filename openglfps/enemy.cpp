#include "enemy.h"

enemy::enemy(std::vector<unsigned int> f, int i) : frames(f), id(i) {
	cs.r = 5.0;
}
void enemy::show() {
	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	glRotatef(rotation.x, 1, 0, 0);
	glRotatef(rotation.y, 0, 1, 0);
	glRotatef(rotation.z, 0, 0, 1);
	glScalef(1, 1, 1);
	glCallList(frames[curframe]);
	glPopMatrix();
}