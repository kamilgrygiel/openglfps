#ifndef WEAPON_H
#define WEAPON_H
#include <SDL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <vector>
#include <string>
#include "vector3d.h"
#include "collisionplane.h"
#include "collisionsphere.h"
#include "camera.h"
#include "collision.h"

class weapon {
	std::vector<unsigned int> frames;
	unsigned int outerview;
	unsigned int normalanimation;
	unsigned int fireanimation;
	unsigned int reloadanimation;
	vector3d position_expected;
	vector3d rotation_expected;

	vector3d position;
	vector3d rotation;

	vector3d aimposition;
	vector3d aimrotation;

	vector3d curpos;
	vector3d currot;

	float precision, aimprecision;
	int strength;
	bool isaim, isreloading, isautomatic, isfired, istest;

	unsigned int maxBulletsInMag;
	unsigned int bulletsInMag;
	unsigned int allbullets;

	unsigned int lastshot;
	unsigned int speed;

	unsigned int curframe;
	unsigned int curmode;

	std::string name;

public:
	weapon(std::vector<unsigned int>& anim, unsigned int ol,
		unsigned int na, unsigned int fa, unsigned int ra,
		vector3d pos, vector3d rot, vector3d apos,
		vector3d arot, float prec, float aprec, int str,
		int maxb, int allbul, int sp, const char* nam,
		bool isa);
	~weapon();
	void update();
	void show();
	bool fire(vector3d& direction, vector3d camdirection);
	void stopfire();
	void reload();
	void aim();
	void addBullets(unsigned int num);
	void setBullets(unsigned int num);
	int getStrength();
	int getAmmo();
	int getAllAmmo();
	std::string getName();
	std::vector<unsigned int>& getAnimation();
	bool isAimed();
	unsigned int getOuterView();
	
	
};

#endif 

