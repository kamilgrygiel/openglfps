#ifndef ENEMY_H
#define ENEMY_H
#include <SDL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <vector>
#include "objloader.h"
#include "vector3d.h"
#include "collisionplane.h"
#include "collisionsphere.h"
#include "camera.h"
#include "functions.h"
#include "collision.h"
#include "level.h"
#include "player.h"
#include "weapon.h"
#include "zombie.h"
#include "network.h"

struct enemy{
	int id;
	std::vector<unsigned int> frames;
	collisionsphere cs;
	int curframe;
	vector3d position;
	vector3d rotation;
	vector3d lookDirection;
	int health;
	weapon* curweapon;
	enemy(std::vector<unsigned int> f, int i);
	void show();

};
#endif 

